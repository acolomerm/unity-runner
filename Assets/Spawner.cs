using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour

   
{
    public GameObject plataforma;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(crearEnemigo());
    }

    // Update is called once per frame
    void Update()
    {

    }
    IEnumerator crearEnemigo()
    {
        while (true)
        {
            GameObject newPlataforma = Instantiate(plataforma);
            newPlataforma.transform.position = new Vector2(this.transform.position.x, Random.Range(-3.41f, 2.0f));
            //esto es lo que esperas es lo mismo que un Thread.Sleep
            int tiempoEspera = Random.Range(1, 3);
            yield return new WaitForSeconds(tiempoEspera);
        }
    }

}                

